
(function maze(id = 0, size = 90, width = 15, height = 15) {

	{
		let state = 0;

		const STATES = Object.freeze({ ACCEPTED: 'accepted', KILLED: 'killed', ENDED: 'ended' });


		window.onload = async function()
		{
			try
			{
				await (async function configure()
				{
					global:
					{
						application = new PIXI.Application();

						PIXI.Sprite.prototype.tag = 'untaged';
						PIXI.Sprite.prototype.stretch = null;


						gameObjects = new Map();

						gameObjects.set('gameObjects', []);
						gameObjects.set('maze', []);

						gameObjects.findLocation = function(entity)
						{
							const maze = gameObjects.get('maze');

							for (const location in maze)
								if (maze[location].has(entity.name)) return maze[location];

							return undefined;
						};


						handler = Object.freeze
						({
							_initialized: new Map ([[ 'x' , false ], [ 'y' , false ]]),

							set: function(pony, property, value)
							{
								switch ((handler._initialized.has(property) && !handler._initialized.get(property)) ? handler._initialized.set(property, true) : property)
								{
									case 'x':
									{
										window.scrollTo(value - ( 2 * pony.width ) , pony.y - ( 2 * pony.height ));
										pony.texture = PIXI.Texture.from(gameObjects.findLocation(pony).get('walkable-path').reaction);
										break;
									}
									case 'y':
									{
										window.scrollTo(pony.x - ( 2 * pony.width ) , value - ( 2 * pony.height ));
										pony.texture = PIXI.Texture.from(gameObjects.findLocation(pony).get('walkable-path').reaction);
										break;
									}
								}

								pony[property] = value;
								return true;
							}

						});
					};


					await new Promise(function mazeRequest(resolve)
					{
						const mazeRequest = new XMLHttpRequest(), parameters = Object.seal({ 'maze-width': width, 'maze-height': height, 'maze-player-name': "Pinkie Pie", 'difficulty': 10 });

						mazeRequest.open('POST', 'https://ponychallenge.trustpilot.com/pony-challenge/maze');
						mazeRequest.setRequestHeader('content-type', 'application/json');


						mazeRequest.onload = function()
						{
							let response;

							if (mazeRequest.statusText != 'OK' || typeof ( response = JSON.parse(mazeRequest.response) ) != 'object') throw Error('Error!');


							const requestProperties = new Set(['maze_id']), values = requestProperties.values();

							for (const property of requestProperties)
								if (!response.hasOwnProperty(property)) throw Error('Error!');


							id = response[values.next().value];


							resolve();
						};


						mazeRequest.send(JSON.stringify(parameters));
					});


					await new Promise(function printRequest(resolve)
					{
						const printRequest = new XMLHttpRequest();

						printRequest.open('GET', 'https://ponychallenge.trustpilot.com/pony-challenge/maze/' + id + '/print');


						printRequest.onload = function()
						{
							let response;

							if (printRequest.statusText != 'OK' || typeof (response = printRequest.response) != 'string') throw Error('Error!');

							console.log( 'https://ponychallenge.trustpilot.com' + '\n\n' + RegExp('[A-Z]', 'g')[Symbol.replace](response, ' ') );


							const mappings = new Map();

							const TAG = Object.freeze({ STATIC: 'static', DYNAMIC: 'dynamic' }), STRETCH = Object.freeze({ SMALL: 0.9, LARGE: 1.5 });

							mappings.set( Symbol('api.(+)'),	{ 'name' : "wall-1", 'stretch' : STRETCH.SMALL }							);	// key[0]
							mappings.set( Symbol('api.(|)'),	{ 'name' : "wall-2", 'stretch' : STRETCH.SMALL }							);	// key[1]
							mappings.set( Symbol('api.(---)'),	{ 'name' : "wall-3", 'stretch' : STRETCH.LARGE }							);	// key[2]
							mappings.set( Symbol('api.( )'),	{ 'name' : "transition-vertical", 'stretch' : STRETCH.SMALL }				);	// key[3]
							mappings.set( Symbol('api.(   )'),	{ 'name' : "transition-horizontal", 'stretch' : STRETCH.LARGE }				);	// key[4]
							mappings.set( Symbol('api.(   )'),	{ 'name' : "walkable-path", 'stretch' : STRETCH.LARGE, 'tag' : TAG.STATIC }	);	// key[5]
							mappings.set( Symbol('api.( P )'),	{ 'name' : "pony", 'stretch' : STRETCH.LARGE, 'texture': PIXI.Texture.from('https://i.imgur.com/TAbYY7L.png'), 'handler' : handler, 'tag' : TAG.DYNAMIC }	);	// key[6]
							mappings.set( Symbol('api.( D )'),	{ 'name' : "domokun", 'stretch' : STRETCH.LARGE, 'texture': PIXI.Texture.from('https://i.imgur.com/ITNkbiX.png'), 'tag' : TAG.DYNAMIC }						);	// key[7]
							mappings.set( Symbol('api.( E )'),	{ 'name' : "end-point", 'stretch' : STRETCH.LARGE, 'texture': PIXI.Texture.from('https://i.imgur.com/dmyIngv.png'), 'tag' : TAG.DYNAMIC }					);	// key[8]
							mappings.set( Symbol('api.(\n)'),	{ 'name' : "line-break", 'stretch' : STRETCH.SMALL }						);	// key[9]

							if (!Array.from(mappings.values()).every(entry => ['name', 'stretch'].every(property => property in entry))) throw Error('Error!');

							mappings.set = null;

							mappings.get = function(key)
							{
								const handler = Map.prototype.get.apply(mappings, [key]).handler;

								return (!!handler) ?

									new Proxy(Object.assign(new PIXI.Sprite(), Map.prototype.get.apply(mappings, [key])), handler) :

									Object.assign(new PIXI.Sprite(), Map.prototype.get.apply(mappings, [key]));
							};


							mapping:
							{
								const keys = Array.from(mappings.keys());

								const SMALL = 1, LARGE = 3;


								mappingloop:

								for (let [position, entry] of RegExp(`(.{${SMALL}})(.{${LARGE}}|\n)`, 'g')[Symbol.split](response).filter(Boolean).entries())
								{
									switch(true)
									{
										case keys[0].description.includes(entry): entry = new Map([ [mappings.get(keys[0]).name, mappings.get(keys[0])] ]); break;
										case keys[1].description.includes(entry): entry = new Map([ [mappings.get(keys[1]).name, mappings.get(keys[1])] ]); break;
										case keys[2].description.includes(entry): entry = new Map([ [mappings.get(keys[2]).name, mappings.get(keys[2])] ]); break;
										case keys[3].description.includes(entry): entry = new Map([ [mappings.get(keys[3]).name, mappings.get(keys[3])] ]); break;
										case (( Math.floor(position / (( 2 * width ) + 2) ) % 2 ) === 0) && keys[4].description.includes(entry): entry = new Map([ [mappings.get(keys[4]).name, mappings.get(keys[4])] ]); break;
										case keys[9].description.includes(entry): entry = new Map([ [mappings.get(keys[9]).name, mappings.get(keys[9])] ]); break;

										default: // walkable-path
										{
											switch(true)
											{
												case (( Math.floor(position / (( 2 * width ) + 2) ) % 2 ) === 1) && keys[5].description.includes(entry): entry = new Map([ [mappings.get(keys[5]).name, mappings.get(keys[5])] ]); break;
												case keys[6].description.includes(entry): entry = new Map([ [mappings.get(keys[5]).name, mappings.get(keys[5])], [mappings.get(keys[6]).name, mappings.get(keys[6])] ]); break;
												case keys[7].description.includes(entry): entry = new Map([ [mappings.get(keys[5]).name, mappings.get(keys[5])], [mappings.get(keys[7]).name, mappings.get(keys[7])] ]); break;
												case keys[8].description.includes(entry): entry = new Map([ [mappings.get(keys[5]).name, mappings.get(keys[5])], [mappings.get(keys[8]).name, mappings.get(keys[8])] ]); break;

												default: throw Error('Error!');
											}

											gameObjects.get('maze').push(entry);
										}
									}

									gameObjects.get('gameObjects').push(entry);

									entry.forEach(element => !gameObjects.get(element.tag) ? gameObjects.set(element.tag, [element]) : gameObjects.get(element.tag).push(element));
								}
							};


							resolve();
						};


						printRequest.send();
					});

				})();


				await (async function initialize()
				{
					(function renderer(resources = [

							new Map // Weird Forest [Region 0]
							([
								[ 'background', 0x608E8C ],
								[ 'wall-1', ['https://i.imgur.com/K2V0Dwj.png', 'https://i.imgur.com/7Raqicn.png', 'https://i.imgur.com/pxEooB0.png', 'https://i.imgur.com/YwUt0My.png'] ],
								[ 'wall-2', ['https://i.imgur.com/jsbUZ2a.png', 'https://i.imgur.com/SxRufPl.png', 'https://i.imgur.com/OeDluEP.png'] ],
								[ 'wall-3', ['https://i.imgur.com/RmHkCTF.png', 'https://i.imgur.com/Xokpi5Q.png', 'https://i.imgur.com/hbrQaIj.png', 'https://i.imgur.com/Df9euz3.png', 'https://i.imgur.com/GCtvv7M.png', 'https://i.imgur.com/4Lhbans.png'] ]
							]),

							new Map // Green Forest [Region 1]
							([
								[ 'background', 0x85A13D ],
								[ 'wall-1', ['https://i.imgur.com/mOqp958.png', 'https://i.imgur.com/DvlhQIE.png', 'https://i.imgur.com/VxcZ0TV.png', 'https://i.imgur.com/byftbfR.png', 'https://i.imgur.com/NEf3OXa.png', 'https://i.imgur.com/UQgpPZj.png', 'https://i.imgur.com/Js0lhct.png'] ],
								[ 'wall-2', ['https://i.imgur.com/btglguR.png', 'https://i.imgur.com/XwQgHmo.png', 'https://i.imgur.com/FkAj7Xn.png', 'https://i.imgur.com/qDPezlv.png', 'https://i.imgur.com/cuAGwR2.png', 'https://i.imgur.com/UJseNd2.png', 'https://i.imgur.com/LwGgHsN.png', 'https://i.imgur.com/WmRkVFr.png'] ],
								[ 'wall-3', ['https://i.imgur.com/S4hSGUi.png', 'https://i.imgur.com/92gWeM8.png', 'https://i.imgur.com/MIwTEgD.png', 'https://i.imgur.com/iEtg97h.png', 'https://i.imgur.com/uPJ5PjX.png', 'https://i.imgur.com/s6PJ8De.png', 'https://i.imgur.com/8yG5dFb.png'] ]
							]),

							new Map // Dark Forest [Region 2]
							([
								[ 'background', 0x001F4B ],
								[ 'wall-1', ['https://i.imgur.com/WSqrMuE.png', 'https://i.imgur.com/SgkqAAr.png', 'https://i.imgur.com/lPzXtqS.png', 'https://i.imgur.com/634nCH8.png'] ],
								[ 'wall-2', ['https://i.imgur.com/LWWndKJ.png', 'https://i.imgur.com/zAtB7wP.png', 'https://i.imgur.com/tSbgsYM.png' ] ],
								[ 'wall-3', ['https://i.imgur.com/3stqbXE.png', 'https://i.imgur.com/L2UzjMC.png', 'https://i.imgur.com/l8hXqfW.png', 'https://i.imgur.com/2Tgi9ID.png', 'https://i.imgur.com/Q4WPe8K.png', 'https://i.imgur.com/VvTM04o.png'] ]					
							]),

							new Map // Spring Forest [Region 3]
							([
								[ 'background', 0xD8BF7D ],
								[ 'wall-1', ['https://i.imgur.com/wLu6ITE.png', 'https://i.imgur.com/htbvmQe.png', 'https://i.imgur.com/eVeAbrd.png'] ],
								[ 'wall-2', ['https://i.imgur.com/hU6b8lf.png', 'https://i.imgur.com/YLC9dVD.png', 'https://i.imgur.com/gvN82k8.png'] ],						
								[ 'wall-3', ['https://i.imgur.com/YP9XWut.png', 'https://i.imgur.com/u3cd7cJ.png', 'https://i.imgur.com/iJxrUvi.png', 'https://i.imgur.com/yGuySt4.png', 'https://i.imgur.com/BExV296.png', 'https://i.imgur.com/4W1OPaA.png'] ]					
							])

						])
					{
						let x = 0, y = 0, region; // [0..3]

						const reactions = new Map([[0, 'https://i.imgur.com/R6ZLdZS.png'],[1, 'https://i.imgur.com/TAbYY7L.png'],[2, 'https://i.imgur.com/1RZBOvq.png'],[3, 'https://i.imgur.com/C8OSXrP.png']]);


						renderloop:

						for (const [position, entry] of gameObjects.get('gameObjects').entries())
						{
							if (position === (2 * width * (y + 1)) + ((2 * y) + 1)) // line-break
							{
								x = 0, y++;

								continue renderloop;
							}

							entry.forEach(element =>
							{
								element.width = size * element.stretch;
								element.height = size;


								element.x = x;
								element.y = y * element.height;


								if (!element.texture.baseTexture.resource)
								{
									region = ((Math.floor(position / (width + 1)) % 2)) + ((Math.floor(y / (height + 1)) * 2)), assets = resources[region];


									texture:
									{
										let textures;

										if (!(textures = assets.get(element.name))) break texture;

										element.texture = PIXI.Texture.from(textures[Math.floor(Math.random() * textures.length)]);
									};


									background:
									{
										const background = new PIXI.Sprite();


										background.width = size * element.stretch;
										background.height = size;


										background.x = x;
										background.y = y * background.height;


										background.texture = PIXI.Texture.WHITE;
										background.tint = assets.get('background');


										application.stage.addChild(background);
									};
								}


								application.stage.addChild(element);
							});


							if (entry.has('walkable-path')) entry.get('walkable-path').reaction = reactions.get(region);

							x += entry.values().next().value.width;
						}

					})();


					await new Promise(function stateRequest(resolve)
					{
						const stateRequest = new XMLHttpRequest();

						stateRequest.open('GET', 'https://ponychallenge.trustpilot.com/pony-challenge/maze/' + id);


						stateRequest.onload = function()
						{
							let response;

							if (stateRequest.statusText != 'OK' || typeof ( response = JSON.parse(stateRequest.response) ) != 'object') throw Error('Error!');


							const requestProperties = new Set(['pony', 'end-point', 'data']), values = requestProperties.values();

							for (const property of requestProperties)
								if (!response.hasOwnProperty(property)) throw Error('Error!');


							const { [values.next().value]: start, [values.next().value]: end, [values.next().value]: walls } = response;


							const type = [];
							
							type.push(Array.isArray(start) 	? true : false);
							type.push(Array.isArray(end) 	? true : false);
							type.push(Array.isArray(walls) 	? true : false);

							if (!type.every(property => property === true )) throw Error('Error!');


							help(start.pop(), end.pop(), walls);


							resolve();
						};


						stateRequest.send();
					});


					window:
					{
						const lastElement = application.stage.children[application.stage.children.length - 1];

						application.renderer.resize(lastElement.x + lastElement.width, lastElement.y + lastElement.height);
						document.body.appendChild(application.view);


						const pony = gameObjects.get('dynamic').find(entity => entity.name === 'pony');

						window.scrollTo(pony.x - (2 * pony.width) , pony.y - (2 * pony.height));
						pony.texture = PIXI.Texture.from(gameObjects.findLocation(pony).get('walkable-path').reaction);


						window.addEventListener('keydown', move);
					};

				})();
			}

			catch(error) { console.log(error); }
		}


		async function move(keyEvent, directionKeys = new Map([

			[ 'W' , { direction : "north", anchor : 0 }		],
			[ 'A' , { direction : "west", anchor : 1 }		],
			[ 'S' , { direction : "south", anchor : 1 }		],
			[ 'D' , { direction : "east", anchor : 0 }		]

			]))
		{
			if (!directionKeys.has(String.fromCharCode(keyEvent.keyCode))) return false;

			window.removeEventListener('keydown', move);


			try
			{
				await new Promise(function moveRequest(resolve)
				{
					const moveRequest = new XMLHttpRequest();

					moveRequest.open('POST', 'https://ponychallenge.trustpilot.com/pony-challenge/maze/' + id);
					moveRequest.setRequestHeader('content-type', 'application/json');


					moveRequest.onload = async function()
					{
						if (moveRequest.statusText != 'OK')
						{
							window.addEventListener('keydown', move);
							throw Error('Error!');
						}


						turn:
						{
							const pony = gameObjects.get('dynamic').find(entity => entity.name === 'pony');

							const current = directionKeys.get(String.fromCharCode(keyEvent.keyCode))['anchor'];

							if (pony.anchor.x != current)
							{
								pony.anchor.set(current, 0); 
								pony.scale.x *= -1;
							}
						};


						await update();


						switch (RegExp(Object.values(STATES).join('|'), 'gi')[Symbol.match](state).toString())
						{
							case STATES.ACCEPTED:
							{
								await new Promise(function stateRequest(resolve)
								{
									const stateRequest = new XMLHttpRequest();

									stateRequest.open('GET', 'https://ponychallenge.trustpilot.com/pony-challenge/maze/' + id);


									stateRequest.onload = function()
									{
										if (stateRequest.statusText != 'OK') throw Error('Error!');


										const requestProperties = new Set(['pony', 'end-point', 'data']), values = requestProperties.values();


										const { [values.next().value]: start, [values.next().value]: end, [values.next().value]: walls } = JSON.parse(stateRequest.response);


										help(start.pop(), end.pop(), walls);


										resolve();
									};


									stateRequest.send();
								});

								window.addEventListener('keydown', move);
								break;
							}

							case STATES.KILLED:
							{
								setTimeout(function(){ document.body.insertAdjacentHTML('afterbegin', '<div style="position:fixed; width:100%; height:100%; z-index:2; background-color: rgba(0,0,0,0.5);"><img style="max-width: 100%; max-height: 100%; display: block; margin: 0 auto;" src="https://ponychallenge.trustpilot.com/eW91X2tpbGxlZF90aGVfcG9ueQ==.jpg"></div>'); }, 500);
								document.body.style.overflow = 'hidden';
								return;
							}

							case STATES.ENDED:
							{
								setTimeout(function(){ document.body.insertAdjacentHTML('afterbegin', '<div style="position:fixed; width:100%; height:100%; z-index:2; background-color: rgba(0,0,0,0.5);"><img style="max-width: 100%; max-height: 100%; display: block; margin: 0 auto;" src="https://ponychallenge.trustpilot.com/eW91X3NhdmVkX3RoZV9wb255.jpg"></div>'); }, 500);
								document.body.style.overflow = 'hidden';
								return;
							}

							default: throw Error('Error!');
						}


						resolve();
					};


					moveRequest.send(JSON.stringify(directionKeys.get(String.fromCharCode(keyEvent.keyCode)), ['direction']));
				});
			}

			catch (error) { console.log(error); }
		}


		async function update()
		{
			try
			{
				await new Promise(function stateRequest(resolve)
				{
					const stateRequest = new XMLHttpRequest();

					stateRequest.open('GET', 'https://ponychallenge.trustpilot.com/pony-challenge/maze/' + id);


					stateRequest.onload = function()
					{
						let response;

						if (stateRequest.statusText != 'OK' || typeof ( response = JSON.parse(stateRequest.response) ) != 'object') throw Error('Error!');

						if (!gameObjects.get('dynamic').every(entity => entity.name in response)) throw Error('Error');


						update:
						{
							let entities = Object.entries(response).filter(([key]) => gameObjects.get('dynamic').find(entity => entity.name === key));

								entities = entities.map(([key, value]) => [ gameObjects.get('dynamic').find(entity => entity.name === key), value.pop() ]);


							const maze = gameObjects.get('maze');

							updateloop:

							for (const [entity, position, location = gameObjects.findLocation(entity)] of entities.filter(([entity, position]) => maze.indexOf(gameObjects.findLocation(entity)) != position))
							{
								location.delete(entity.name);

								maze[position].set(entity.name, entity);

								
								setCoordinates:
								{
									const wp = maze[position].get('walkable-path');

									application.stage.setChildIndex(entity, application.stage.getChildIndex(wp) + 1);
									
									Math.floor(position / width) === Math.floor(maze.indexOf(location) / width) ? entity.x = wp.x : entity.y = wp.y;
								};


								collision:
								{
									const pony = gameObjects.get('dynamic').find(entity => entity.name === 'pony'), location = gameObjects.findLocation(pony);

									const dynamic = gameObjects.get('dynamic');


									switch (true)
									{
										case location.has('domokun'):
										{
											state = STATES.KILLED;

											application.stage.setChildIndex(dynamic.find(entity => entity.name === 'domokun'),
												application.stage.getChildIndex(pony) + 1);

											break updateloop;
										}
										
										case location.has('end-point'):
										{
											state = STATES.ENDED;

											application.stage.setChildIndex(pony,
												application.stage.getChildIndex(dynamic.find(entity => entity.name === 'end-point')) + 1);

											break;
										}

										default:
											state = STATES.ACCEPTED;
											break;
									}
								};
							}
						};


						resolve();
					};


					stateRequest.send();
				});
			}

			catch (error) { console.log(error); }
		}
	}


	{
		const guidingPath = [];


		function help(start, end, walls)
		{
			const maze = gameObjects.get('maze');


			checkCurrentLocation:
			{
				const location = maze[start];

				if (location.has('trail'))
				{
					application.stage.removeChild(location.get('trail'));
					location.delete('trail');


					guidingPath.shift();

					return;
				}
				else
				{
					guidingPath.forEach(trail =>
					{
						application.stage.removeChild(maze[trail].get('trail'));
						maze[trail].delete('trail');
					});
						

					guidingPath.length = 0;
				}
			};


			const stack = [start], WALL = Object.freeze({ NORTH: 'north', WEST: 'west'});

			depthFirstSearch :

			do
			{
				const position = stack[stack.length - 1];


				if (position === end) break; else guidingPath.push(position);


				// UP
				// DOWN
				// LEFT
				// RIGHT


				let deadEnd = false;

				( ( Math.floor(position / width) != 0			) && ( !walls[position].includes(WALL.NORTH)		) && ( !guidingPath.includes(position - width)	) ) ? stack.push(position - width)	: deadEnd = true;
				( ( Math.floor(position / width) != (height - 1)) && ( !walls[position + width].includes(WALL.NORTH)) && ( !guidingPath.includes(position + width)	) )	? stack.push(position + width)	: deadEnd = true;
				( ( Math.floor(position % width) != 0			) && ( !walls[position].includes(WALL.WEST)			) && ( !guidingPath.includes(position - 1)		) )	? stack.push(position - 1)		: deadEnd = true;
				( ( Math.floor(position % width) != (width - 1)	) && ( !walls[position + 1].includes(WALL.WEST)		) && ( !guidingPath.includes(position + 1)		) )	? stack.push(position + 1)		: deadEnd = true;


				if (deadEnd) // backtrack to last the common position (node) shared by the stack and the guidingPath when there is a dead end
				{
					const difference = stack.length - guidingPath.length;

					for (let i = stack.length - 1; i >= 0; i--)
					{
						if (stack[i] != guidingPath[i - difference])
						{
							guidingPath.length = (i - difference) + 1;
							stack.length = i + 1;

							continue depthFirstSearch;
						}
					}
				}


			} while (stack.length != 0)


			createPath:
			{
				guidingPath.shift();

				guidingPath.forEach(position =>
				{
					const wp = maze[position].get('walkable-path'), { x, y, width, height } = wp;

					const trail = Object.assign(new PIXI.Sprite(PIXI.Texture.from('https://i.imgur.com/YSviDmq.png')), { x, y, width, height });


					application.stage.addChildAt(trail, application.stage.getChildIndex(wp));
					maze[position].set('trail', trail);
				});
			};
		}
	}

})();